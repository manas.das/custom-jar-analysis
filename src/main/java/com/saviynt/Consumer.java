package com.saviynt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Consumer extends Thread {
    private Logger logger = LogManager.getLogger(Consumer.class);
    private final BlockingQueue<String> queue;
    private final String outputFileName;
    private AtomicBoolean active = new AtomicBoolean(true);


    public Consumer(BlockingQueue<String> queue, String outputFileName) {
        this.queue = queue;
        this.outputFileName = outputFileName;
    }

    public void shutdown() {
        active.set(false);
    }

    @Override
    public void run() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName))) {
            String line;
            while(active.get()){
                //line = queue.take();
                line = queue.poll(1000, TimeUnit.MILLISECONDS);
                if(line!=null){
                    writer.write(line);
                    writer.newLine();
                }
            }
            //finally cleaning up the remaining of the queue
            while (!queue.isEmpty()){
                line = queue.poll(1000, TimeUnit.MILLISECONDS);
                if(line!=null){
                    writer.write(line);
                    writer.newLine();
                }
            }
        } catch (IOException | InterruptedException e) {
            logger.error("Exception in Consumer Thread", e);
        }
    }
}
