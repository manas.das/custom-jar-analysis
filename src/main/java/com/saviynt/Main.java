package com.saviynt;

import com.github.kwart.jd.JavaDecompiler;
import com.github.kwart.jd.input.JDInput;
import com.github.kwart.jd.input.ZipFileInput;
import com.github.kwart.jd.options.DecompilerOptions;
import com.github.kwart.jd.output.DirOutput;
import com.github.kwart.jd.output.JDOutput;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException {

        Main m = new Main();
        //load properties
        final Properties properties = m.loadProperties();
        //Getting list of external Jar Files
        Path extJarDirPath = Paths.get(properties.getProperty ("ext.jar.path"));
        Path decompileDirPath = Paths.get(properties.getProperty ("ext.jar.decompile.path"));
        final List<Path> jarPaths = m.listFiles(extJarDirPath, new String[] {""}, new String[] {"jar"} );
        Path tempDir = Files.createTempDirectory(decompileDirPath, "decompiled");
        //Decompiling all the jar files
        jarPaths.forEach(path -> {
            try {
                m.decompile(path, tempDir);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        final String parentPath = tempDir.toString();
        String[] searchFor = {"DriverManager.getConnection"
                //        ,"externalconfig.properties", "Class.forName(\"com.mysql.jdbc.Driver\")"
        };
        String[] excludeJarNames = {"ojdbc", "mysql-connector", "db2jcc", "postgres", "nzjdbc", "jconn4"
                , "habeans", "postgis-jdbc", "MySqlJdbcDriver", "Text_JDBC"};
        String[] excludeFileExtensions = {"properties"};
        String[] excludePkgNs = {
                "/com/ibm/as400"
                , "/com/ibm/mm/sdk"
                , "/com/ibm/db2"
                , "/org/apache"
                , "/oracle/jdbc/rowset"
                , "/com/mysql"
                , "/com/ibm/eNetwork"
                //, "/com/mysql/cj"
                //, "/com/mysql/jdbc"
                //, "/org/apache/commons"
                //, "/org/apache/log4j"
                //, "/org/apache/ibatis"
                //, "/org/apache/logging"
        };

        int parentPathCharLength = parentPath.length();
        Path path = Paths.get(parentPath);

        List<Path> paths = m.listFiles(path, excludeFileExtensions);

        //Blocking Queue for writing files from parallel thread
        BlockingQueue<String> blockingQueue = new LinkedBlockingQueue<>();
        Consumer consumer = new Consumer(blockingQueue, tempDir.toString()+".json");
        consumer.start();
        paths.parallelStream().forEach(x -> m.searchTokens(searchFor, x, parentPathCharLength, excludeJarNames, excludePkgNs, blockingQueue));
        consumer.shutdown();

        //Granular control on number of parallel threads
        /*ForkJoinPool customThreadPool = new ForkJoinPool(4);
        try {
            customThreadPool.submit(() -> paths.parallelStream().forEach(x -> m.searchTokens(searchFor, x, parentPathCharLength, excludeJarNames, excludePkgNs, blockingQueue)););
        } finally {
            customThreadPool.shutdown();
        }*/

    }

    private Properties loadProperties(){
        Properties prop = null;
        try (InputStream input = Main.class.getClassLoader().getResourceAsStream("config.properties")) {
            prop = new Properties();
            if (input == null) {
                logger.error("Sorry, unable to find config.properties");
                return null;
            }
            //load a properties file from class path, inside static method
            prop.load(input);
            return prop;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }

    /*private Properties loadYamlProperties(){
        Properties prop = null;
        Yaml yaml = new Yaml();
        try (InputStream input = Main.class.getClassLoader().getResourceAsStream("config.properties")) {
            prop = new Properties();
            Map<String, Object> yamlMap = yaml.load(input);

            // Convert the YAML map to properties
            convertYAMLToProperties("", yamlMap, prop);
            if (input == null) {
                logger.error("Sorry, unable to find config.properties");
                return null;
            }
            return prop;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }*/

    /*private static void convertYAMLToProperties(String parentKey, Map<String, Object> yamlMap, Properties properties) {
        for (Map.Entry<String, Object> entry : yamlMap.entrySet()) {
            String key = parentKey.isEmpty() ? entry.getKey() : parentKey + "." + entry.getKey();
            Object value = entry.getValue();

            if (value instanceof Map) {
                @SuppressWarnings("unchecked")
                Map<String, Object> subMap = (Map<String, Object>) value;
                convertYAMLToProperties(key, subMap, properties);
            } else {
                properties.put(key, value.toString());
            }
        }
    }*/

    public List<Path> listFiles(Path directoryPath, String[] excludeFileExtensions, String[] includeFileExtensions) throws IOException {
        List<Path> result;
        //Path directoryPath = Paths.get(path);

        try (Stream<Path> walk = Files.walk(directoryPath)) {
            result = walk
                    .filter(Files::isRegularFile)
                    .filter(p -> Arrays.stream(excludeFileExtensions)
                            .filter(extn -> !extn.isEmpty())
                            .noneMatch(extn -> p.getFileName().toString().toLowerCase().endsWith(extn)))
                    .filter(p -> Arrays.stream(includeFileExtensions)
                            .filter(extn -> !extn.isEmpty())
                            .anyMatch(extn -> p.getFileName().toString().toLowerCase().endsWith(extn)))
                    .collect(Collectors.toList());
        }
        return result;
    }

    public List<Path> listFiles(Path path, String[] excludeFileExtensions) throws IOException {
        List<Path> result;
        try (Stream<Path> walk = Files.walk(path)) {
            result = walk
                    .filter(Files::isRegularFile)
                    .filter(p -> Arrays.stream(excludeFileExtensions).noneMatch(extn -> p.getFileName().toString().toLowerCase().endsWith(extn)))
                    .collect(Collectors.toList());
        }
        return result;
    }

    private void decompile(Path jarPath, Path decompilePath) throws IOException {

        String jarFileNameWExtn = jarPath.getFileName().toString();
        //Get jar file name without extension - ##Start##
        String jarFileName = null;
        int lastIndex = jarFileNameWExtn.lastIndexOf('.');
        if (lastIndex > 0) {
            jarFileName = jarFileNameWExtn.substring(0, lastIndex);
        }
        //Get jar file name without extension - ##End##

        JDInput input = new ZipFileInput(jarPath.toString());
        JDOutput output = new DirOutput(new File(decompilePath.toString()+"/"+jarFileName+".jar"));
        JavaDecompiler decompiler = new JavaDecompiler(new DecompilerOptions() {
            @Override
            public boolean isSkipResources() {return false;}
            @Override
            public boolean isEscapeUnicodeCharacters() {return false;}
            @Override
            public boolean isDisplayLineNumbers() {return true;}
            @Override
            public boolean isParallelProcessingAllowed() {return true;}
        });
        input.decompile(decompiler, output);
        //Copy the jar files at decompiled location. Uncommenting the following will result in error because jar content is getting unarchived under the same directory name.
        //Files.copy(jarPath, Paths.get(String.valueOf(decompilePath), jarFileNameWExtn), StandardCopyOption.REPLACE_EXISTING);
    }


    public String getPackageNameSpaceForJavaFile(String pathStartWithCustom, String fileExtension, int jarNameEndIndex) {
        if (fileExtension != null && fileExtension.equalsIgnoreCase("java")) {
            int pnsEnd = pathStartWithCustom.lastIndexOf("/");
            if (pnsEnd != -1 && pnsEnd < pathStartWithCustom.length() - 1) {
                return pathStartWithCustom.substring(jarNameEndIndex, pnsEnd);
            }
        }
        return null;
    }

    public String getTenantName(String absPathStr, int parentPathCharLength) {
        try {
            int endIndexTenant = absPathStr.indexOf("/", parentPathCharLength + 1);
            if (endIndexTenant > -1) {
                return absPathStr.substring(parentPathCharLength + 1, endIndexTenant);
            }
            return absPathStr.substring(parentPathCharLength + 1);
        } catch (Exception e) {
            return null;
        }
    }

    public void searchTokens(String[] searchFor,
                             Path path,
                             int parentPathCharLength,
                             String[] excludeJarNames,
                             String[] excludePkgNs,
                             BlockingQueue<String> blockingQueue) {

        String absPathStr = path.toAbsolutePath().toString();
        //String tenantName = getTenantName(absPathStr, parentPathCharLength);
        String fileName = path.getFileName().toString();
        String fileExtension = null;
        int lastDotIndex = fileName.lastIndexOf('.');
        if (lastDotIndex > 0 && lastDotIndex < fileName.length() - 1) {
            fileExtension = fileName.substring(lastDotIndex + 1);
        }
        String pathStartWithCustom = absPathStr.substring(parentPathCharLength);
        int indexOfJar = pathStartWithCustom.indexOf(".jar");
        String jarPath = null;
        String jarName = null;
        if (indexOfJar != -1) {
            int jarNameBeginIndex = pathStartWithCustom.lastIndexOf("/", indexOfJar) + 1;
            int jarNameEndIndex = pathStartWithCustom.indexOf("/", indexOfJar);
            if (jarNameEndIndex != -1) {
                jarPath = pathStartWithCustom.substring(0, jarNameEndIndex);
                jarName = pathStartWithCustom.substring(jarNameBeginIndex, jarNameEndIndex);
                //Excluding specific jar name
                if (jarName != null && !jarName.isEmpty() && Arrays.stream(excludeJarNames).anyMatch(jarName::contains)) {
                    return;
                }

                //Excluding specific java file under specific package name specifier
                String javaFilePkgNs = getPackageNameSpaceForJavaFile(pathStartWithCustom, fileExtension, jarNameEndIndex);
                if (javaFilePkgNs != null && !javaFilePkgNs.isEmpty() && Arrays.stream(excludePkgNs).anyMatch(javaFilePkgNs::startsWith)) {
                    return;
                }
            }
        }
        try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            String line;
            int lineNumber = 0;
            List<TokenFileOccurrence.TokenOccurrence> fileLevelCollect = new ArrayList<>();

            while ((line = reader.readLine()) != null) {
                lineNumber++;
                String finalLine = line;
                int finalLineNumber = lineNumber;
                List<TokenFileOccurrence.TokenOccurrence> collect = Arrays.stream(searchFor).map(s -> {
                            if (finalLine.contains(s)) {
                                return new TokenFileOccurrence.TokenOccurrence(s, finalLineNumber);
                            }
                            return null;
                        })
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
                if (!collect.isEmpty()) {
                    fileLevelCollect.addAll(collect);
                }
            }

            if (!fileLevelCollect.isEmpty()) {
                TokenFileOccurrence tokenFileOccurrence = new TokenFileOccurrence(absPathStr, /*tenantName,*/ jarPath, jarName, fileLevelCollect);
                new Gson().toJson(tokenFileOccurrence);
                blockingQueue.put(new Gson().toJson(tokenFileOccurrence));
                logger.info("{}", new Gson().toJson(tokenFileOccurrence));
            }

        } catch (IOException e) {
            //logger.error("Error reading the file: {}", path.toAbsolutePath());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }




}